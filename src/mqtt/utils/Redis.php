<?php

namespace WhaleIot\mqtt\utils;
use RedisException;

class Redis
{

    protected $handler = null;
    protected $options
                       = [
            'ip'      => '127.0.0.1',
            'port'    => 6379,
            'auth'    => '',
            'db'      => 0,
            'timeout' => 0,
        ];

    /**
     * 构造函数
     * @param array $options 缓存参数
     * @throws RedisException
     * @access public
     */
    public function __construct(array $options = [])
    {
        if (!extension_loaded('redis')) {
            throw new \BadFunctionCallException('not support: redis');
        }
        if (!empty($options)) {
            $this->options = array_merge($this->options, $options);
        }
        $this->handler = new \Redis;
        $this->handler->connect($this->options['ip'], $this->options['port'], $this->options['timeout']);


        if ('' != $this->options['auth']) {
            $this->handler->auth($this->options['auth']);
        }

        if (0 != $this->options['db']) {
            $this->handler->select($this->options['db']);
        }
    }

    /**
     * 注释:将一个或多个成员元素及其分数值加入到有序集当中
     * 创建者:Wzp
     * 时间:2023/6/29 11:52
     * @param $key
     * @param $score_or_options
     * @param $scores_and_mems
     * @return false|int|\Redis
     * @throws RedisException
     */
    public function zAdd($key, $score_or_options, $scores_and_mems)
    {
        return $this->handler->zAdd($key, $score_or_options, $scores_and_mems);
    }

    /**
     * 注释:将一个或多个值插入到列表头部
     * 创建者:Wzp
     * 时间:2023/6/29 11:53
     * @param $key
     * @param $value
     * @return false|int|\Redis
     * @throws RedisException
     */
    public function lPush($key, $value)
    {
        return $this->handler->lPush($key, $value);
    }
}