<?php

namespace WhaleIot\mqtt\libs;

use Exception;
use RedisException;
use WhaleIot\mqtt\validate\Validate;

class Send
{
    /**
     * 注释:查询端口总数-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 16:50
     * @param string $imei
     * @throws RedisException
     */
    public static function queryPortTotal(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x01,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:查询所有空闲的充电站端口-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 17:52
     * @param string $imei
     * @throws RedisException
     */
    public static function queryPortIdle(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x02,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:查询消费总额数据-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 17:55
     * @param string $imei
     * @throws RedisException
     */
    public static function queryTotalMoney(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x07,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:设置IC卡投币器是否可用-平台发送
     * 创建者:Wzp
     * 时间:2023/6/30 11:44
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function setICBlock(string $imei, array $param)
    {
        $rule  = [
            'coin' => 'require|integer|in:0,1',
            'ic'   => 'require|integer|in:0,1',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x09,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:上传设备故障-平台回复
     * 创建者:Wzp
     * 时间:2023/6/29 17:55
     * @param string $imei
     * @throws RedisException
     */
    public static function recoverFault(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x0A,
            'param' => [
                'state' => 0
            ],
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:锁定或者解锁某一个端口-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 17:55
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function setPortLock(string $imei, array $param)
    {
        $rule  = [
            'port'   => 'require|integer|between:1,255',
            'status' => 'require|integer|in:0,1',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x0C,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:结束设备-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 14:46
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function stopDevices(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
            'type' => 'require|integer|in:0,1,2',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x0D,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:查询设备端口状态-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 17:59
     * @param string $imei
     * @throws RedisException
     */
    public static function devicesPortStatus(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x0F,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:设置系统参数-平台发送
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 02:31
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function setSystemConfig(string $imei, array $param)
    {
        $rule  = [
            'coin_min'         => 'require|integer|between:0,65535',
            'card_min'         => 'require|integer|between:0,65535',
            'coin_elec'        => 'require|integer|between:0,255',
            'card_elec'        => 'require|integer|between:0,255',
            'cst'              => 'require|integer|between:0,255',
            'power_max_1'      => 'require|integer|between:0,65535',
            'power_max_2'      => 'require|integer|between:0,65535',
            'power_max_3'      => 'require|integer|between:0,65535',
            'power_max_4'      => 'require|integer|between:0,65535',
            'power_2_time'     => 'require|integer|between:0,255',
            'power_3_time'     => 'require|integer|between:0,255',
            'power_4_time'     => 'require|integer|between:0,255',
            'sp_rec_mon'       => 'require|integer|in:0,1',
            'sp_full_empty'    => 'require|integer|in:0,1',
            'full_power_min'   => 'require|integer|between:0,255',
            'full_charge_time' => 'require|integer|between:0,255',
            'elec_time_first'  => 'require|integer|in:0,1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x18,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:读取设备系统参数-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 18:01
     * @param string $imei
     * @throws RedisException
     */
    public static function getSystemConfig(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x1E,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:在线刷卡-平台回复
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 02:45
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function recoverIcCard(string $imei, array $param)
    {
        $rule  = [
            'res'       => 'require|integer|in:0,1,2',
            'card_surp' => 'require|number|between:0,6553.5',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x10,
            'param' => $param,
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:查询当前的充电状态-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 18:01
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function getUsingPortStatus(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x15,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:充电结束-平台回复
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 02:45
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function recoverStopDevice(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x16,
            'param' => $param,
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:开启设备-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 14:35
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function startDevices(string $imei, array $param)
    {
        $rule  = [
            'port'  => 'require|integer|between:1,255',
            'money' => 'require|number|between:0,6553.5',
            'time'  => 'require|integer|between:0,65535',
            'elec'  => 'require|number|between:0,655.35',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x14,
            'param' => $param,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:上报刷卡投币打开的信息-平台回复
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 02:45
     * @param string $imei
     * @throws RedisException
     */
    public static function recoverIcCardStart(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x20,
            'param' => ['state' => 0],
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:上传端口的实时状态-平台回复
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 02:45
     * @param string $imei
     * @throws RedisException
     */
    public static function recoverPortStatus(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x21,
            'param' => ['state' => 0],
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:参数设置2-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 14:35
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function setSystemConfig2(string $imei, array $param)
    {
        $rule          = [
            'low_power'            => 'require|integer|between:0,255',
            'max_total_time'       => 'require|integer|between:0,255',
            'over_voltage'         => 'require|integer|between:0,65535',
            'low_voltage'          => 'require|integer|between:0,65535',
            'over_temp'            => 'require|integer|between:0,255',
            'over_temp_delay_time' => 'require|integer|between:0,255',
            'time_disp_on'         => 'require',
            'time_disp_off'        => 'require',
            'price_power_1'        => 'require|integer|between:0,65535',
            'price_power_2'        => 'require|integer|between:0,65535',
            'price_power_3'        => 'require|integer|between:0,65535',
            'price_power_4'        => 'require|integer|between:0,65535',
            'price_power_5'        => 'require|integer|between:0,65535',
            'low_power_check_time' => 'require|integer|between:0,65535',
            'stop_delay_time'      => 'require|integer|between:0,65535',
            'max_total_power'      => 'require|integer|between:0,65535',
        ];
        $time_disp_on  = explode(':', $param['time_disp_on']);
        $time_disp_off = explode(':', $param['time_disp_off']);
        if (count($time_disp_on) != 2) {
            throw new Exception('time_disp_on格式必须是00:00');
        }
        if (count($time_disp_off) != 2) {
            throw new Exception('time_disp_off格式必须是00:00');
        }
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x22,
            'param' => $param,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:参数读取2-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 18:01
     * @param string $imei
     * @throws RedisException
     */
    public static function getSystemConfig2(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x23,
            'param' => [
                'state' => 0
            ]
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:设置音量-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 16:33
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function setVolume(string $imei, array $param)
    {
        $rule  = [
            'volume' => 'require|integer|between:0,8',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x24,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:上线登录-平台回复
     * 创建者:Wzp
     * 时间:2023/6/29 18:01
     * @param string $imei
     * @throws RedisException
     */
    public static function recoverDeviceLogin(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0xA1,
            'param' => [
                'state' => 0
            ],
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:设备心跳-平台回复
     * 创建者:Wzp
     * 时间:2023/6/29 18:01
     * @param string $imei
     * @throws RedisException
     */
    public static function recoverDeviceHeartbeat(string $imei)
    {
        $data = [
            'imei'  => $imei,
            'cmd'   => 0xA2,
            'param' => [
                'state' => 0
            ],
            'sop'   => 0x55,
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:借用开柜-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 16:33
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function openDoorBorrow(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x31,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:归还开柜-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 16:33
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function openDoorReturn(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x32,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:强制开柜-平台发送
     * 创建者:Wzp
     * 时间:2023/6/29 16:33
     * @param string $imei
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function openDoorForce(string $imei, array $param)
    {
        $rule  = [
            'port' => 'require|integer|between:1,255',
        ];
        $v     = new Validate($rule);
        $check = $v->check($param);
        if (!$check) {
            throw new Exception($v->getError());
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => 0x34,
            'param' => $param
        ];
        RedisQueue::send($data);
    }

    /**
     * 注释:自定义指令接口
     * 创建者:Wzp
     * 时间:2023/6/29 16:33
     * @param string $imei
     * @param int $cmd
     * @param array $param
     * @throws RedisException
     * @throws Exception
     */
    public static function other(string $imei, int $cmd, array $param)
    {
        foreach ($param as $value) {
            if (!is_integer($value)) {
                throw new Exception('param的值必须是int类型数组');
            }
            if($value<0||$value>255){
                throw new Exception('数值范围0-255');
            }
        }
        $data = [
            'imei'  => $imei,
            'cmd'   => $cmd,
            'param' => $param
        ];
        RedisQueue::send($data);
    }


}