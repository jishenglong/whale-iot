<?php

namespace WhaleIot\mqtt\events;

class Event
{
    /**
     * 注释:设备在线
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 01:48
     * @param string $imei 设备IMEI
     */
    public static function onConnect(string $imei)
    {

    }

    /**
     * 注释:设备离线
     * 创建者:JSL
     * 时间:2023/07/01 001 下午 01:49
     * @param string $imei 设备IMEI
     */
    public static function onClose(string $imei)
    {

    }

    /**
     * 注释:数据回调方法
     * 创建者:JSL
     * @param string $imei 设备IMEI
     * @param array $data 充电桩数据 json结构
     */
    public static function onMessage(string $imei, array $data)
    {

    }

    /**
     * 注释:日志回调方法
     * 创建者:JSL
     * 时间:2023/06/29 029 下午 02:47
     * @param string $imei
     * @param array $data
     */
    public static function onLog(string $imei, array $data)
    {

    }
}