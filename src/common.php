<?php
if (!function_exists('getWhaleIotConfig')) {
    /**
     * 注释:获取配置
     * 创建者:JSL
     * 时间:2023/06/30 030 上午 10:28
     * @param string $name
     * @return array
     */
    function getWhaleIotConfig(string $name = ''): array
    {
        //laravel
        if (class_exists('\Illuminate\Support\Facades\Config')) {
            return \Illuminate\Support\Facades\Config::get($name);
        }
        //tp6
        if (class_exists('\think\facade\Config')) {
            return \think\facade\Config::get($name);
        }
        //tp5
        if (class_exists('\think\Config')) {
            return \think\Config::get($name);
        }
        $config = include dirname(__DIR__) . '/config/whale-iot.php';
        if (empty($name)) {
            return $config;
        }
        if (!strpos($name, '.')) {
            return $config;
        }
        $name = explode('.', $name, 2);
        return $config[$name[1]];
    }
}
